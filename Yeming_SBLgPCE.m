nparam = 5;
noutput = 32;
% sample_idx = 1:6993;  % use all training points
sample_idx = 1:8:6993;
% sample_idx = 1:(1+10+50+180+560);
% sample_idx = 802:1632;

rom_path = '/global/homes/y/yaning/repos/rbf/rom_class';
SBL_path = '/global/homes/y/yaning/repos/rbf/SB2_Release_200';
addpath(genpath(rom_path));
addpath(SBL_path);

data_path = '/global/homes/y/yaning/repos/gpau/scripts/python_scripts/sparse_grid/';

data_tra_fn = [data_path 'SG_Points.txt'];
data_val_fn = {[data_path 'parameters.txt'] [data_path 'outs_Modflow.txt']};

data_tra = dlmread(data_tra_fn);
param_tra = data_tra(sample_idx, 1:nparam);
output_tra = data_tra(sample_idx, nparam+1:end);
clear data_tra;
param_val = dlmread(data_val_fn{1});
output_val = dlmread(data_val_fn{2});

ns_tra = size(param_tra, 1);
ns_val = size(param_val, 1);

param_bound = [0.13 0.17; 55, 57; 5, 50; 0, 3.5; 100, 450];
param_nlz_tra = (param_tra-repmat(param_bound(:,1)', ns_tra, 1))./ ...
    repmat((param_bound(:,2)'-param_bound(:,1)'), ns_tra, 1);
param_nlz_val = (param_val-repmat(param_bound(:,1)', ns_val, 1))./ ...
    repmat((param_bound(:,2)'-param_bound(:,1)'), ns_val, 1);


%%--------------Need to change -------------------------
pce_order = 10;
%%--------------Need to change -------------------------
trunc_rule = 'TD';
basis_type = 'legendre';
method = 'BCS';
diagnostic_level = 2;
monitor = 10;
% time_BCS = 1.e10;  %% infinitely large for BCS time tolerance
%%--------------Need to change -------------------------
tol_BCS = 1.0E-4;  % this number is of no use Need to set SB2_ControlSettings.m
%%--------------Need to change -------------------------
fixed_noise = false;
max_iter = 10000;


%%--------------------------------if use POD----------------------------------
% kl_size = 32;
% tol_kl = 1e-3;
% kl = karhunen_loeve(kl_size);
% kl.construct(output_tra');
% eig_err = (1-cumsum(max(0,kl.eigenvalues/kl.eig_total)));
% idx_min_eig_err = find(eig_err < tol_kl);
% M = min([idx_min_eig_err(1), kl_size, ns_tra]);
% %%--------------Need to change -------------------------
% % M = 4;
% %%--------------Need to change -------------------------
% [xs_tra] = kl.project(output_tra', M, [], true);
% [xs_val] = kl.project(output_val', M, [], true);
%
% pool_size = 8;
% pp = parpool(pool_size);
%
% pce1 = pce_SBL_mo(nparam, M, pce_order, trunc_rule, basis_type, method);
% pce1.set_SBL_options(SBL_path, fixed_noise, max_iter, tol_BCS, ...
%                                diagnostic_level, monitor);
% fprintf('Constructing PCE for mixing coefficients');
% basis_train = pce1.construct(param_nlz_tra, xs_tra');
% [xs_pred, basis_val] = pce1.pce_prediction([], param_nlz_val);
% [err, err_norm] = pce1.compute_error(xs_val', xs_pred, ...
%                                      true, 'rmse');
% fprintf('Mean error for mixing coefficients is # %8.4f\n', mean(err, 1));
%%-----------------------------------------------------------------------

%%--------------------------------if not use POD----------------------------------
pool_size = 8;
pp = parpool(pool_size);

pce1 = pce_SBL_mo(nparam, noutput, pce_order, trunc_rule, basis_type, method);
pce1.set_SBL_options(SBL_path, fixed_noise, max_iter, tol_BCS, ...
                               diagnostic_level, monitor);
fprintf('Constructing PCE for mixing coefficients');
basis_train = pce1.construct(param_nlz_tra, output_tra);
[output_pred, basis_val] = pce1.pce_prediction([], param_nlz_val);
[err, err_norm] = pce1.compute_error(output_val, output_pred, ...
                                     true, 'rmse');
fprintf('Mean error for mixing coefficients is # %8.4f\n', mean(err, 1));

%% if use POD-------------------------------------------------------------
%% POD Reconstruction
% output_pred = kl.basis(:, 1:M)*xs_pred' + repmat(kl.ref_basis, 1, ns_val);
% output_pred = output_pred';
%
% %% POD validation
% norm = sum(output_val.^2, 1);
% norm(norm<1.e-6) = size(output_val, 2);
% err_pod = sqrt(sum((output_pred-output_val).^2, 1)./norm);
%%-------------------------------------------------------------------------

corr = corrcoef(output_val(:), output_pred(:));

err_output = zeros(noutput, 1);
for i = 1 : noutput
    err_output(i) = sqrt(sum(((output_pred(:,i)-output_val(:,i))./...
                              output_val(:,i)).^2)/ns_val);
end

%% Ming's SG predicted output
data_SG_fn = [data_path 'outs_SG.txt'];
output_SG = dlmread(data_SG_fn);
err_SG = zeros(noutput, 1);
for i = 1 : noutput
    err_SG(i) = sqrt(sum(((output_SG(:,i)-output_val(:,i))./...
                          output_val(:,i)).^2)/ns_val);
end

delete(gcp('nocreate'))

%disp('Saving results...')
%%%--------------Need to change -------------------------
%% save(['/global/cscratch1/sd/yaning/Research/Liange_sim_new/PODfSBLgPCE_mo_' varn ...
%%       '_results_tra1234val1po' num2str(pce_order) 'tol4nb' num2str(M)],...
%%      'pce1', 'kl', 'xs_tra', 'xs_val', 'output_pred', 'output_val', 'plume_val', 'plume_pred', ...
%%      'err','err_pod', 'corr', 'err_plume', 'volume_masked', 'volume', 'mask', '-v7.3');
%save([data_path '/PODfSBLgPCE_mo_' varn ...
%
%val', 'plume_pred', ...
%', '-v7.3');