nparam = 5;
noutput = 32;
sample_idx = 1:6993;  % use all training points
% sample_idx = 1:8:6993;
% sample_idx = 1:(1+10+50+180+560);
% sample_idx = 802:1632;

rom_path = '/global/homes/y/yaning/repos/rbf/rom_class';
% rom_path = '/Users/yaningliu/GoogleDrive/repos/rbf/rom_class';
addpath(genpath(rom_path));

data_path = '/global/homes/y/yaning/repos/gpau/scripts/python_scripts/sparse_grid/';
% data_path = '/Users/yaningliu/GoogleDrive/repos/gpau/scripts/python_scripts/sparse_grid/';

data_tra_fn = [data_path 'SG_Points.txt'];
data_val_fn = {[data_path 'parameters.txt'] [data_path 'outs_Modflow.txt']};

param_bound = [0.13 0.17; 55, 57; 5, 50; 0, 3.5; 100, 450];

sg2 = sparse_grid2(5, 6, 'UF');
sg2.construct_sg2();

param_yaning = repmat(param_bound(:,1)', sg2.grid_size, 1) + ...
    sg2.grids.*(repmat((param_bound(:,2)'-param_bound(:,1)'), ...
    sg2.grid_size, 1));

data_tra = dlmread(data_tra_fn);
param_tra = data_tra(sample_idx, 1:nparam);
output_tra = data_tra(sample_idx, nparam+1:end);
clear data_tra;
param_val = dlmread(data_val_fn{1});
output_val = dlmread(data_val_fn{2});

ns_tra = size(param_tra, 1);
ns_val = size(param_val, 1);

param_nlz_tra = (param_tra-repmat(param_bound(:,1)', ns_tra, 1))./ ...
    repmat((param_bound(:,2)'-param_bound(:,1)'), ns_tra, 1);
param_nlz_val = (param_val-repmat(param_bound(:,1)', ns_val, 1))./ ...
    repmat((param_bound(:,2)'-param_bound(:,1)'), ns_val, 1);

idx = zeros(ns_tra, 1);
for i = 1 : ns_tra
    [~, idx(i)] = ismembertol(param_tra(i,:), param_yaning, ...
                              1e-6, 'ByRows', 1);
end
