nparam = 5;
noutput = 32;
level_max = 4;
sg_size = [1 11 61 241 801 2433 6993 19313 51713];
sample_idx = 1: sg_size(level_max+1);
ipl_bf_n = 2;

% sample_idx = 1:6993;  % use all training points
% sample_idx = 1:8:6993;
sample_idx = 1:(1+10+50+180+560);
% sample_idx = 802:1632;


% rom_path = '/global/homes/y/yaning/repos/rbf/rom_class';
rom_path = '/Users/yaningliu/GoogleDrive/repos/rbf/rom_class';
addpath(genpath(rom_path));

% data_path = '/global/homes/y/yaning/repos/gpau/scripts/python_scripts/sparse_grid/';
data_path = '/Users/yaningliu/GoogleDrive/repos/gpau/scripts/python_scripts/sparse_grid/';

data_tra_fn = [data_path 'SG_Points.txt'];
data_val_fn = {[data_path 'parameters.txt'] [data_path 'outs_Modflow.txt']};

data_tra = dlmread(data_tra_fn);
param_tra = data_tra(sample_idx, 1:nparam);
output_tra = data_tra(sample_idx, nparam+1:end);
clear data_tra;
param_val = dlmread(data_val_fn{1});
output_val = dlmread(data_val_fn{2});

ns_tra = size(param_tra, 1);
ns_val = size(param_val, 1);

param_bound = [0.13 0.17; 55, 57; 5, 50; 0, 3.5; 100, 450];
param_nlz_tra = (param_tra-repmat(param_bound(:,1)', ns_tra, 1))./ ...
    repmat((param_bound(:,2)'-param_bound(:,1)'), ns_tra, 1);
param_nlz_val = (param_val-repmat(param_bound(:,1)', ns_val, 1))./ ...
    repmat((param_bound(:,2)'-param_bound(:,1)'), ns_val, 1);

save_fn_tra = 'data_tra.txt';
save_fn_val = 'data_val.txt';

fileID = fopen(save_fn_tra, 'w');
data = [param_nlz_tra output_tra]';
formatSpec = ['%16.10f %16.10f %16.10f %16.10f %16.10f %16.10f %16.10f ' ...
              '%16.10f %16.10f %16.10f %16.10f %16.10f %16.10f %16.10f ' ...
              '%16.10f %16.10f %16.10f %16.10f %16.10f %16.10f %16.10f ' ...
              '%16.10f %16.10f %16.10f %16.10f %16.10f %16.10f %16.10f ' ...
              '%16.10f %16.10f %16.10f %16.10f %16.10f %16.10f %16.10f ' ...
              '%16.10f %16.10f\n'];
fprintf(fileID, formatSpec, data);
fclose(fileID);

fileID = fopen(save_fn_val, 'w');
data = [param_nlz_val output_val]';
formatSpec = ['%16.10f %16.10f %16.10f %16.10f %16.10f %16.10f %16.10f ' ...
              '%16.10f %16.10f %16.10f %16.10f %16.10f %16.10f %16.10f ' ...
              '%16.10f %16.10f %16.10f %16.10f %16.10f %16.10f %16.10f ' ...
              '%16.10f %16.10f %16.10f %16.10f %16.10f %16.10f %16.10f ' ...
              '%16.10f %16.10f %16.10f %16.10f %16.10f %16.10f %16.10f ' ...
              '%16.10f %16.10f\n'];
fprintf(fileID, formatSpec, data);
fclose(fileID);


asgc = ASGC(nparam, level_max, 'UF');
asgc.construct_sg_eff();
sg_pred = asgc.sg_interpolate(ipl_bf_n, save_fn_tra, save_fn_val);

rrmse = zeros(noutput, 1);
for i = 1 : noutput
    rrmse(i) = sqrt(sum(((sg_pred(:,i)-output_val(:,i))./output_val(:,i)).^2)/ns_val);
end